package core.jms;

import ch.qos.logback.classic.pattern.MessageConverter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.*;

@Component
public class JmsPublisher {
    Logger logger = LoggerFactory.getLogger(JmsPublisher.class);

    @Autowired
    ActiveMQConnectionFactory connectionFactory;

    @Autowired
    Destination topic;


    public void produceMessage(MyMessage message){
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, 1);

            MessageProducer producer = session.createProducer(topic);

            producer.send(session.createObjectMessage(message));
        } catch (JMSException e) {
            logger.error("Error publishing MyMessage:" + message, e);
        }
    }
}
