package core;

import ch.qos.logback.classic.pattern.MessageConverter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    @Value("${activemq.brokerUrl}")
    private String brokerUrl;

    @Value("${activemq.topicName}")
    private String topicName;

    public static void main(String[]args){
        SpringApplication.run(Application.class);
    }

    @Bean
    ActiveMQConnectionFactory connectionFactory(){
        return new ActiveMQConnectionFactory(brokerUrl);
    }

    @Bean
    ActiveMQTopic destination(){
        return new ActiveMQTopic(topicName);
    }
}
