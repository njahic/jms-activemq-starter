package core.jms;

import java.io.Serializable;

public class MyMessage implements Serializable {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
