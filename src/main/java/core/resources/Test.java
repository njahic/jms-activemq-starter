package core.resources;

import core.jms.JmsPublisher;
import core.jms.MyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {

    @Autowired
    JmsPublisher publisher;

    @RequestMapping("/notify")
    public MyMessage notify(@RequestParam("text") String text){
        MyMessage msg = new MyMessage();
        msg.setText(text);
        publisher.produceMessage(msg);

        return msg;
    }
}
